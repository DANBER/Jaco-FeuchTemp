## intent:read_temperature
- Wie (kalt|heiß|warm) ist es (hier|)?
- Wie viel Grad hat es?

## intent:read_humidity
- Was ist die Luftfeuchte?
- Wie niedrig ist die Feuchtigkeit?
- Wie hoch ist die Luftfeuchtigkeit?
- Wie feucht ist es?
