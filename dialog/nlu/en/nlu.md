## intent:read_temperature
- How (warm|cold|hot) is it (here|)?
- How many degrees is it?

## intent:read_humidity
- What is the air humidity?
- How (low|high) is the humidity?
- How humid is it?
