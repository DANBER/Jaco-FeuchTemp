# Jaco-FeuchTemp

Ist deine Wohnung eine Sauna oder ein feuchtes Verließ? \
Liest Temperatur und Feuchtigkeit über einen DHT-Sensor aus.

<br>

Hardware-Setup: https://www.einplatinencomputer.com/raspberry-pi-temperatur-und-luftfeuchtigkeitssensor-dht22/

<br>

What **you can learn** in this skill:

- Access to gpio pins

**Complexity**: Medium

<br>

Build and run skill solely for debugging purposes: \
(Assumes mqtt-broker already running)

```bash
sudo docker run --security-opt label=disable --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx build --platform=linux/arm64 -t skill_jaco_feuchtemp_arm64 - < skills/skills/Jaco-FeuchTemp/Containerfile_arm64

docker run --network host --rm \
  --volume `pwd`/skills/skills/Jaco-FeuchTemp/:/Jaco-Master/skills/skills/Jaco-FeuchTemp/:ro \
  --volume `pwd`/skills/skills/Jaco-FeuchTemp/skilldata/:/Jaco-Master/skills/skills/Jaco-FeuchTemp/skilldata/ \
  --volume `pwd`/userdata/config/:/Jaco-Master/userdata/config/:ro \
  --privileged \
  -it skill_jaco_feuchtemp_raspbian python3 /Jaco-Master/skills/skills/Jaco-FeuchTemp/action-humtemp.py
```
